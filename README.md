# CRUD BOOK STORE

Restful API Toko Buku untuk Backend Developer Test

## Instalisasi

Pertama, clone repositori menggunakan git.

```bash
git clone git@gitlab.com:arisnacg/crud-book-store.git
```

Kedua, masuk ke dalam folder.

```bash
cd crud-book-store
```

Install dependencies yang diperlukan.

```bash
npm install
```

## Konfigurai

Buatlah database baru dengan nama `bookstore` atau nama database boleh terserah Anda. Buka file `ormconfig.json` dan ganti `database` sesuai dengan nama database yang akan digunakan

```json
{
  "name": "default",
  "type": "mysql",
  "host": "localhost",
  "port": 3306,
  "username": "root",
  "password": "",
  "database": "bookstore" //nama database,
  "synchronize": true,
  "logging": true,
  "entities": ["src/models/*.ts"]
}
```

Saat aplikasi dijalankan, tabel-tabel model yang berada di folder `models` akan secara otomotis dibuat pada database.

## Menjalankan Aplikasi

- `npm start`
- Restful API kemudian dapat dites pada url `http://localhost:8000`
- CURD Buku pada url `http://localhost:8000/book`
- CURD Penulis pada url `http://localhost:8000/author`
