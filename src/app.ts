import "reflect-metadata"

import Express from "express"
import { createConnection } from "typeorm"
import bodyParser from "body-parser"
import { router } from "./routes/index"

//main
const main = async () => {
  await createConnection()
  const app = Express()
  app.use(bodyParser.urlencoded({ extended: false }))
  app.use("/", router)
  const port = 8000
  app.listen(port, () => {
    console.log(`Server started on http://localhost:${port}`)
  })
}

main()
