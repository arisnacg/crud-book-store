import { Request, Response } from "express"
import { Author } from "../models/Author"
import { Book } from "../models/Book"

export let read = async (req: Request, res: Response) => {
  res.json(await Author.find({ relations: ["books"] }))
}

export let findOne = async (req: Request, res: Response) => {
  let author = await Author.findOne({ where: { id: Number(req.params.id) }, relations: ["books"] })
  if (author)
    res.json(author)
  else
    res.status(404).json({
      status: false,
      message: "Author not found"
    })
}

export let create = async (req: Request, res: Response) => {
  let newAuthor = new Author()
  newAuthor.name = req.body.name
  await newAuthor.save()
  res.status(201).json({
    status: true,
    message: "Author has been sucessfully created"
  })
}

export let update = async (req: Request, res: Response) => {
  let author = await Author.findOne({ id: Number(req.params.id) })
  if (author) {
    author.name = req.body.name
    await author.save()
    res.json({
      status: true,
      message: "Author has been sucessfully updated"
    })
  } else {
    res.status(404).json({
      status: false,
      message: "Author not found"
    })
  }
}

export let destroy = async (req: Request, res: Response) => {
  let author = await Author.findOne({ id: Number(req.params.id) })
  if (author) {
    await Book.delete({ authorId: author.id })
    await author.remove()
    res.json({
      status: true,
      message: "Author has been sucessfully deleted with his/her books"
    })
  } else {
    res.status(404).json({
      status: false,
      message: "Author not found"
    })
  }
}