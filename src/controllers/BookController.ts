import { Request, Response } from "express"
import { Book } from "../models/Book"

export let read = async (req: Request, res: Response) => {
  res.json(await Book.find({ relations: ["author"] }))
}

export let findOne = async (req: Request, res: Response) => {
  let book = await Book.findOne({ where: { id: Number(req.params.id) }, relations: ["author"] })
  if (book)
    res.json(book)
  else
    res.status(404).json({
      status: false,
      message: "Book not found"
    })
}

export let create = async (req: Request, res: Response) => {
  let newBook = new Book()
  newBook.title = req.body.title
  newBook.description = req.body.description
  newBook.authorId = req.body.authorId
  newBook.price = Number(req.body.price)
  newBook.publishedDate = req.body.publishedDate
  await newBook.save()

  res.status(201).json({
    status: true,
    message: "Book has been sucessfully created"
  })
}

export let update = async (req: Request, res: Response) => {
  let book = await Book.findOne({ id: Number(req.params.id) })
  if (book) {
    book.title = req.body.title
    book.description = req.body.description
    book.authorId = req.body.authorId
    book.price = req.body.price
    book.publishedDate = req.body.publishedDate
    await book.save()
    res.json({
      status: true,
      message: "Book has been sucessfully updated"
    })
  } else {
    res.status(404).json({
      status: false,
      message: "Book not found"
    })
  }
}

export let destroy = async (req: Request, res: Response) => {
  let book = await Book.findOne({ id: Number(req.params.id) })
  if (book) {
    await book.remove()
    res.json({
      status: true,
      message: "Book has been sucessfully deleted"
    })
  } else {
    res.status(404).json({
      status: false,
      message: "Book not found"
    })
  }
}

