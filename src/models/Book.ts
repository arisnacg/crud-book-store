import { BaseEntity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, Entity, ManyToOne } from "typeorm"
import { Author } from "./Author"

@Entity()
export class Book extends BaseEntity {
  @PrimaryGeneratedColumn()
  readonly id: number

  @Column({ nullable: false })
  title: string

  @Column("text")
  description: string

  @Column()
  authorId: number

  @ManyToOne(() => Author, author => author.books)
  author: Author

  @Column("double")
  price: number

  @Column("date")
  publishedDate: Date

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date
}