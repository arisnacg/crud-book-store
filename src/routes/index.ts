import { Router } from "express"
import { authorValidationRules, bookValidationRules, validate } from "../validation/index"
import * as BookController from "../controllers/BookController"
import * as AuthorController from "../controllers/AuthorController"

export const router = Router()

router.get("/book", BookController.read)
router.get("/book/:id", BookController.findOne)
router.post("/book", bookValidationRules(), validate, BookController.create)
router.put("/book/:id", bookValidationRules(), validate, BookController.update)
router.delete("/book/:id", BookController.destroy)

router.get("/author", AuthorController.read)
router.get("/author/:id", AuthorController.findOne)
router.post("/author", authorValidationRules(), validate, AuthorController.create)
router.put("/author/:id", authorValidationRules(), validate, AuthorController.update)
router.delete("/author/:id", AuthorController.destroy)
