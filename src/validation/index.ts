const { body, validationResult } = require('express-validator')
import { Request, Response, NextFunction } from "express"
import { Author } from "../models/Author"

export let bookValidationRules = () => {
  return [
    body("title").notEmpty().withMessage("required"),
    body("price").isFloat({ min: 0 }),
    body('publishedDate').isDate(),
    body("authorId").custom(async (value) => {
      const user = await Author.findOne({ id: value })
      if (user)
        return true
      else
        throw new Error("Author not found")
    })
  ]
}

export let authorValidationRules = () => {
  return [
    body("name").notEmpty().withMessage("required")
  ]
}

export let validate = (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req)
  if (errors.isEmpty()) {
    return next()
  }
  let extractedErrors: any = []
  errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))

  return res.status(422).json({
    errors: extractedErrors,
  })
}